object SelectForm: TSelectForm
  Left = 342
  Top = 223
  BorderStyle = bsDialog
  Caption = #36873#25321#31526#21512#26465#20214#30340#21830#21697
  ClientHeight = 316
  ClientWidth = 566
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 275
    Width = 566
    Height = 41
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 0
  end
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 0
    Width = 566
    Height = 275
    Align = alClient
    DataSource = dsSelect
    DynProps = <>
    Flat = True
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    TabOrder = 1
    TitleParams.RowHeight = 20
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'SP_CODE'
        Footers = <>
        Title.Alignment = taCenter
        Width = 69
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'SP_NAME'
        Footers = <>
        Title.Alignment = taCenter
        Width = 95
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'BAR_CODE'
        Footers = <>
        Title.Alignment = taCenter
        Width = 97
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'UNIT'
        Footers = <>
        Title.Alignment = taCenter
        Width = 59
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'REG_NAME'
        Footers = <>
        Title.Alignment = taCenter
        Width = 59
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'SALE_PRICE'
        Footers = <>
        Title.Alignment = taCenter
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'W_ID'
        Footers = <>
        Title.Alignment = taCenter
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object dsSelect: TDataSource
    DataSet = MainFrm.cdsTmp
    Left = 128
    Top = 184
  end
end
