unit unMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls,
  DynVarsEh, GridsEh, DBAxisGridsEh, DBGridEh, ToolWin, DB, DBClient,
  ExtCtrls, StdCtrls, ActnList, frxClass, frxDBSet, frxBarcode, frxDesgn, unTools;

type
  TMainFrm = class(TForm)
    StatusBar: TStatusBar;
    grid: TDBGridEh;
    dsGrid: TDataSource;
    cdsGrid: TClientDataSet;
    Panel1: TPanel;
    Label1: TLabel;
    edtCode: TEdit;
    Button1: TButton;
    cdsGridSP_ID: TIntegerField;
    cdsGridSP_CODE: TStringField;
    cdsGridSP_NAME: TStringField;
    cdsGridSHORT_NAME: TStringField;
    cdsGridSPEC: TStringField;
    cdsGridBAR_CODE: TStringField;
    cdsGridHH: TStringField;
    cdsGridUNIT: TStringField;
    cdsGridREG_NAME: TStringField;
    cdsGridSALE_PRICE: TCurrencyField;
    cdsGridW_ID: TIntegerField;
    cdsGridBULK_FLAG: TBooleanField;
    ActionList1: TActionList;
    actGet: TAction;
    frxReport: TfrxReport;
    frxDBDataset: TfrxDBDataset;
    frxBarCodeObject1: TfrxBarCodeObject;
    frxDesigner: TfrxDesigner;
    actDesign: TAction;
    actPrint: TAction;
    actLoad: TAction;
    GroupBox1: TGroupBox;
    cbbulk: TCheckBox;
    Label2: TLabel;
    edtFS: TEdit;
    UpDown1: TUpDown;
    Button2: TButton;
    actClear: TAction;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    actExit: TAction;
    Button6: TButton;
    cbPreview: TCheckBox;
    OpenDialog: TOpenDialog;
    cbstyle: TComboBox;
    Button7: TButton;
    actHelp: TAction;
    cdsTmp: TClientDataSet;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    CurrencyField1: TCurrencyField;
    IntegerField2: TIntegerField;
    BooleanField1: TBooleanField;
    procedure actGetExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure actPrintExecute(Sender: TObject);
    procedure actClearExecute(Sender: TObject);
    procedure actLoadExecute(Sender: TObject);
    procedure actDesignExecute(Sender: TObject);
    procedure actExitExecute(Sender: TObject);
    procedure actHelpExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    { Private declarations }
    ReportFile: String;
  public
    { Public declarations }
  end;

var
  MainFrm: TMainFrm;

implementation

uses unDm, unSelect, unHelp;

{$R *.dfm}

procedure TMainFrm.actGetExecute(Sender: TObject);
var
  vData: OleVariant;
  SQLStr: String;
  id: integer;
begin
  if Length(edtCode.Text) > 0 then
  begin
    with UserDm.qryGet do
    begin
      case cbstyle.ItemIndex of
        0: begin
             Close;
             SQL.Clear;
             SQL.Add('select SP_ID from Sp_Info where SP_CODE=:CODE');
             Params[0].AsString := edtCode.Text;
             Prepare;
             Open;
           end;
        1: begin
             Close;
             SQL.Clear;
             SQL.Add('select SP_ID from Sp_Barcode where BAR_CODE=:ID');
             Params[0].AsString := edtCode.Text;
             Prepare;
             Open;
           end;
        2: begin
             Close;
             SQL.Clear;
             SQL.Add('select SP_ID from Sp_Weigh where W_ID=:ID');
             Params[0].AsInteger := StrToInt(edtCode.Text);
             Prepare;
             Open;
           end;
      end;

      if RecordCount = 0 then
      begin
        edtCode.SelectAll;
        edtCode.SetFocus;
        Exit;
      end;

      id := FieldByName('SP_ID').AsInteger;

      Close;
      SQL.Clear;
      SQL.Add('select a.SP_ID, SP_CODE, SP_NAME, SHORT_NAME, SPEC, BAR_CODE, HH, UNIT, REG_NAME, SALE_PRICE, d.W_ID, a.BULK_FLAG');
      SQL.Add('from Sp_Info a, Region c, Sp_Weigh d');
      SQL.Add('where a.AREA_CODE=c.AREA_CODE and a.SP_ID*=d.SP_ID');
      SQL.Add('and a.SP_ID=:ID');
      ParamByName('ID').AsInteger := id;
      Prepare;
      Open;
    
      First;
      While Not Eof do
      begin
        cdsGrid.Append;
        cdsGridSP_ID.AsInteger := FieldByName('SP_ID').AsInteger;
        cdsGridSP_CODE.AsString := FieldByName('SP_CODE').AsString;
        cdsGridSP_NAME.AsString := FieldByName('SP_NAME').AsString;
        cdsGridSHORT_NAME.AsString := FieldByName('SHORT_NAME').AsString;
        cdsGridSPEC.AsString := FieldByName('SPEC').AsString;
        cdsGridBAR_CODE.AsString := FieldByName('BAR_CODE').AsString;
        cdsGridHH.AsString := FieldByName('HH').AsString;
        cdsGridUNIT.AsString := FieldByName('UNIT').AsString;
        cdsGridREG_NAME.AsString := FieldByName('REG_NAME').AsString;
        cdsGridW_ID.AsInteger := FieldByName('W_ID').AsInteger;
        cdsGridBULK_FLAG.AsBoolean := FieldByName('BULK_FLAG').AsBoolean;

        if (cbBulk.Checked) and (FieldByName('BULK_FLAG').AsBoolean) then
        begin
          cdsGridUNIT.AsString := '500g';
          cdsGridSALE_PRICE.AsCurrency := FieldByName('SALE_PRICE').AsCurrency/2;
        end else
        begin
          cdsGridUNIT.AsString := FieldByName('UNIT').AsString;
          cdsGridSALE_PRICE.AsCurrency := FieldByName('SALE_PRICE').AsCurrency;
        end;
        cdsGrid.Post;
        Next;
      end;
    end;

    edtCode.Clear;
    edtCode.SetFocus;
  end else
  begin
    ShowMessage('查询编码不能为空！');
    edtCode.setFocus;
  end;
end;

procedure TMainFrm.FormCreate(Sender: TObject);
begin
  UserDm := TUserDm.Create(Self);
  MainFrm.Caption := Application.Title;
  try
    UserDm.LSDB.Open;
    UserDm.Connected := True;
  Except
    MainFrm.Caption := MainFrm.Caption + '(脱机状态)';
    UserDm.Connected := False;
  end;

  actGet.Enabled := UserDm.Connected;

end;

procedure TMainFrm.edtCodeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 013 then
    actGet.Execute;
end;

procedure TMainFrm.FormShow(Sender: TObject);
begin
  edtCode.SetFocus;
  cdsGrid.CreateDataSet;
  cdsTmp.CreateDataSet;
  cdsGrid.Open;
end;

procedure TMainFrm.actPrintExecute(Sender: TObject);
var
  vData, vData1: OleVariant;
  fs, i: integer;
begin
  if (ReportFile = '') or (ReportFile = null) then
  begin
    ShowMessage('请先选择一种价签格式文件!');
    Exit;
  end;

  fs := StrToInt(edtFS.Text);

  if not cdsTmp.Active then
    cdsTmp.Open;

  cdsTmp.EmptyDataSet;

  cdsGrid.First;
  while Not cdsGrid.Eof do
  begin
    for i := 0 to (fs - 1) do
    begin
      cdsTmp.Append;
      cdsTmp.FieldByName('SP_ID').AsInteger := cdsGrid.FieldByName('SP_ID').AsInteger;
      cdsTmp.FieldByName('SP_CODE').AsString := cdsGrid.FieldByName('SP_CODE').AsString;
      cdsTmp.FieldByName('SP_NAME').AsString := cdsGrid.FieldByName('SP_NAME').AsString;
      cdsTmp.FieldByName('SHORT_NAME').AsString := cdsGrid.FieldByName('SHORT_NAME').AsString;
      cdsTmp.FieldByName('SPEC').AsString := cdsGrid.FieldByName('SPEC').AsString;
      cdsTmp.FieldByName('BAR_CODE').AsString := cdsGrid.FieldByName('BAR_CODE').AsString;
      cdsTmp.FieldByName('HH').AsString := cdsGrid.FieldByName('HH').AsString;
      cdsTmp.FieldByName('UNIT').AsString := cdsGrid.FieldByName('UNIT').AsString;
      cdsTmp.FieldByName('REG_NAME').AsString := cdsGrid.FieldByName('REG_NAME').AsString;
      cdsTmp.FieldByName('SALE_PRICE').AsCurrency := cdsGrid.FieldByName('SALE_PRICE').AsCurrency;
      cdsTmp.FieldByName('W_ID').AsInteger := cdsGrid.FieldByName('W_ID').AsInteger;
      cdsTmp.FieldByName('BULK_FLAG').AsBoolean := cdsGrid.FieldByName('BULK_FLAG').AsBoolean;
      cdsTmp.Post;
    end;
    cdsGrid.Next;
  end;


  frxReport.PrepareReport();
  if cbPreview.Checked then
    frxReport.ShowPreparedReport
  else begin
    frxReport.PrintOptions.ShowDialog := true;
    frxReport.Print;
  end;
  cdsGrid.EnableControls;
end;

procedure TMainFrm.actClearExecute(Sender: TObject);
begin
  if cdsGrid.RecordCount > 0 then
  begin
    if BFChoiceDlg('提示', '您确认要清空价签打印列表吗？') then
    begin
      cdsGrid.EmptyDataSet;
      edtCode.SetFocus;
    end;
  end;
end;

procedure TMainFrm.actLoadExecute(Sender: TObject);
begin
  with OpenDialog do
  begin
    if Execute then
    begin
       ReportFile := FileName;
       frxReport.LoadFromFile(ReportFile, true);
       StatusBar.Panels[1].Text := ExtractFileName(ReportFile);
       StatusBar.Panels[3].Text := FloatToStr(frxReport.Pages[1].Width);
       StatusBar.Panels[5].Text := FloatToStr(frxReport.Pages[1].Height);
    end;
  end;
end;

procedure TMainFrm.actDesignExecute(Sender: TObject);
begin
  frxReport.DesignReport();
end;

procedure TMainFrm.actExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TMainFrm.actHelpExecute(Sender: TObject);
begin
  HelpForm := THelpForm.Create(Self);
  HelpForm.ShowModal;
  HelpForm.Free;
end;

procedure TMainFrm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := BFChoiceDlg('提示', '您确定要退出吗？');
end;

end.
