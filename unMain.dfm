object MainFrm: TMainFrm
  Left = 372
  Top = 201
  Width = 798
  Height = 592
  Caption = 'MainFrm'
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Scaled = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 12
  object StatusBar: TStatusBar
    Left = 0
    Top = 539
    Width = 790
    Height = 19
    Panels = <
      item
        Text = #24403#21069#36733#20837#20215#31614#26684#24335
        Width = 110
      end
      item
        Width = 200
      end
      item
        Alignment = taCenter
        Text = #23485#24230
        Width = 50
      end
      item
        Width = 50
      end
      item
        Alignment = taCenter
        Text = #39640#24230
        Width = 50
      end
      item
        Width = 50
      end>
  end
  object grid: TDBGridEh
    Left = 0
    Top = 85
    Width = 790
    Height = 454
    Align = alClient
    AutoFitColWidths = True
    DataSource = dsGrid
    DynProps = <>
    Flat = True
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh, gioShowRecNoEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghDblClickOptimizeColWidth, dghDialogFind, dghShowRecNo, dghColumnResize, dghColumnMove, dghExtendVertLines]
    SortLocal = True
    TabOrder = 1
    TitleParams.Font.Charset = GB2312_CHARSET
    TitleParams.Font.Color = clWindowText
    TitleParams.Font.Height = -12
    TitleParams.Font.Name = #23435#20307
    TitleParams.Font.Style = []
    TitleParams.ParentFont = False
    TitleParams.RowHeight = 20
    Columns = <
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'SP_CODE'
        Footers = <>
        Title.Alignment = taCenter
        Width = 73
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'SP_NAME'
        Footers = <>
        Title.Alignment = taCenter
        Width = 85
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'SHORT_NAME'
        Footers = <>
        Title.Alignment = taCenter
        Width = 75
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'SPEC'
        Footers = <>
        Title.Alignment = taCenter
        Width = 69
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'BAR_CODE'
        Footers = <>
        Title.Alignment = taCenter
        Width = 89
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'HH'
        Footers = <>
        Title.Alignment = taCenter
        Width = 58
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'UNIT'
        Footers = <>
        Title.Alignment = taCenter
        Width = 41
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'REG_NAME'
        Footers = <>
        Title.Alignment = taCenter
        Width = 66
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'SALE_PRICE'
        Footers = <>
        Title.Alignment = taCenter
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'W_ID'
        Footers = <>
        Title.Alignment = taCenter
      end
      item
        DynProps = <>
        EditButtons = <>
        FieldName = 'BULK_FLAG'
        Footers = <>
        Title.Alignment = taCenter
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 790
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 7
      Top = 14
      Width = 60
      Height = 12
      Caption = #26597#25214#26041#24335#65306
    end
    object edtCode: TEdit
      Left = 151
      Top = 10
      Width = 101
      Height = 20
      TabOrder = 0
      OnKeyDown = edtCodeKeyDown
    end
    object Button1: TButton
      Left = 265
      Top = 8
      Width = 75
      Height = 25
      Action = actGet
      TabOrder = 1
    end
    object Button2: TButton
      Left = 441
      Top = 8
      Width = 75
      Height = 25
      Action = actClear
      TabOrder = 2
    end
    object Button3: TButton
      Left = 521
      Top = 8
      Width = 75
      Height = 25
      Action = actDesign
      TabOrder = 3
    end
    object Button4: TButton
      Left = 601
      Top = 8
      Width = 75
      Height = 25
      Action = actLoad
      TabOrder = 4
    end
    object Button5: TButton
      Left = 345
      Top = 8
      Width = 75
      Height = 25
      Action = actPrint
      TabOrder = 5
    end
    object Button6: TButton
      Left = 689
      Top = 8
      Width = 75
      Height = 25
      Action = actExit
      TabOrder = 6
    end
    object cbstyle: TComboBox
      Left = 72
      Top = 10
      Width = 73
      Height = 20
      Style = csDropDownList
      ItemHeight = 12
      ItemIndex = 0
      TabOrder = 7
      Text = #25353#32534#30721
      Items.Strings = (
        #25353#32534#30721
        #25353#26465#30721
        #25353#31204#30721
        #25353#25171#21253#30721)
    end
    object Button7: TButton
      Left = 768
      Top = 8
      Width = 17
      Height = 25
      Action = actHelp
      TabOrder = 8
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 41
    Width = 790
    Height = 44
    Align = alTop
    Caption = #25171#21360#36873#39033
    TabOrder = 3
    object Label2: TLabel
      Left = 148
      Top = 18
      Width = 48
      Height = 12
      Caption = #25171#21360#20221#25968
    end
    object cbbulk: TCheckBox
      Left = 16
      Top = 16
      Width = 129
      Height = 17
      Caption = #25955#35013#21830#21697#36716#25442#26020#20215
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object edtFS: TEdit
      Left = 204
      Top = 14
      Width = 49
      Height = 20
      TabOrder = 1
      Text = '1'
    end
    object UpDown1: TUpDown
      Left = 253
      Top = 14
      Width = 15
      Height = 20
      Associate = edtFS
      Min = 1
      Position = 1
      TabOrder = 2
    end
    object cbPreview: TCheckBox
      Left = 283
      Top = 16
      Width = 73
      Height = 17
      Caption = #25171#21360#39044#35272
      TabOrder = 3
    end
  end
  object dsGrid: TDataSource
    DataSet = cdsGrid
    Left = 200
    Top = 312
  end
  object cdsGrid: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 232
    Top = 312
    object cdsGridSP_ID: TIntegerField
      DisplayLabel = #21830#21697'ID'
      FieldName = 'SP_ID'
    end
    object cdsGridSP_CODE: TStringField
      DisplayLabel = #21830#21697#32534#30721
      FieldName = 'SP_CODE'
    end
    object cdsGridSP_NAME: TStringField
      DisplayLabel = #21517#31216
      FieldName = 'SP_NAME'
    end
    object cdsGridSHORT_NAME: TStringField
      DisplayLabel = #31616#31216
      FieldName = 'SHORT_NAME'
    end
    object cdsGridSPEC: TStringField
      DisplayLabel = #35268#26684
      FieldName = 'SPEC'
    end
    object cdsGridBAR_CODE: TStringField
      DisplayLabel = #26465#30721
      FieldName = 'BAR_CODE'
    end
    object cdsGridHH: TStringField
      DisplayLabel = #36135#21495
      FieldName = 'HH'
    end
    object cdsGridUNIT: TStringField
      DisplayLabel = #21333#20301
      FieldName = 'UNIT'
    end
    object cdsGridREG_NAME: TStringField
      DisplayLabel = #20135#22320
      FieldName = 'REG_NAME'
    end
    object cdsGridSALE_PRICE: TCurrencyField
      DisplayLabel = #21806#20215
      FieldName = 'SALE_PRICE'
    end
    object cdsGridW_ID: TIntegerField
      DisplayLabel = #31204#30721
      FieldName = 'W_ID'
    end
    object cdsGridBULK_FLAG: TBooleanField
      DisplayLabel = #25955#35013
      FieldName = 'BULK_FLAG'
    end
  end
  object ActionList1: TActionList
    Left = 104
    Top = 208
    object actGet: TAction
      Caption = #26597#35810
      OnExecute = actGetExecute
    end
    object actDesign: TAction
      Caption = #35774#35745#20215#31614
      OnExecute = actDesignExecute
    end
    object actPrint: TAction
      Caption = #25171#21360#20215#31614
      OnExecute = actPrintExecute
    end
    object actLoad: TAction
      Caption = #36733#20837#20215#31614
      OnExecute = actLoadExecute
    end
    object actClear: TAction
      Caption = #28165#31354#21015#34920
      OnExecute = actClearExecute
    end
    object actExit: TAction
      Caption = #36864#20986
      OnExecute = actExitExecute
    end
    object actHelp: TAction
      Caption = '?'
      OnExecute = actHelpExecute
    end
  end
  object frxReport: TfrxReport
    Version = '4.9.32'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    PrintOptions.ShowDialog = False
    ReportOptions.CreateDate = 41610.279457951400000000
    ReportOptions.LastChange = 41616.457162719900000000
    ScriptLanguage = 'PascalScript'
    StoreInDFM = False
    Left = 464
    Top = 256
  end
  object frxDBDataset: TfrxDBDataset
    UserName = 'frxDBDataset1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SP_ID=SP_ID'
      'SP_CODE=SP_CODE'
      'SP_NAME=SP_NAME'
      'SHORT_NAME=SHORT_NAME'
      'SPEC=SPEC'
      'BAR_CODE=BAR_CODE'
      'HH=HH'
      'UNIT=UNIT'
      'REG_NAME=REG_NAME'
      'SALE_PRICE=SALE_PRICE'
      'W_ID=W_ID'
      'BULK_FLAG=BULK_FLAG')
    DataSet = cdsTmp
    BCDToCurrency = False
    Left = 496
    Top = 256
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 528
    Top = 256
  end
  object frxDesigner: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    SaveDir = './labels'
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 560
    Top = 256
  end
  object OpenDialog: TOpenDialog
    Filter = #20215#31614#26684#24335#25991#20214'(*.fr3)|*.fr3'
    InitialDir = '.\labels'
    Left = 368
    Top = 176
  end
  object cdsTmp: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 360
    Top = 312
    object IntegerField1: TIntegerField
      DisplayLabel = #21830#21697'ID'
      FieldName = 'SP_ID'
    end
    object StringField1: TStringField
      DisplayLabel = #21830#21697#32534#30721
      FieldName = 'SP_CODE'
    end
    object StringField2: TStringField
      DisplayLabel = #21517#31216
      FieldName = 'SP_NAME'
    end
    object StringField3: TStringField
      DisplayLabel = #31616#31216
      FieldName = 'SHORT_NAME'
    end
    object StringField4: TStringField
      DisplayLabel = #35268#26684
      FieldName = 'SPEC'
    end
    object StringField5: TStringField
      DisplayLabel = #26465#30721
      FieldName = 'BAR_CODE'
    end
    object StringField6: TStringField
      DisplayLabel = #36135#21495
      FieldName = 'HH'
    end
    object StringField7: TStringField
      DisplayLabel = #21333#20301
      FieldName = 'UNIT'
    end
    object StringField8: TStringField
      DisplayLabel = #20135#22320
      FieldName = 'REG_NAME'
    end
    object CurrencyField1: TCurrencyField
      DisplayLabel = #21806#20215
      FieldName = 'SALE_PRICE'
    end
    object IntegerField2: TIntegerField
      DisplayLabel = #31204#30721
      FieldName = 'W_ID'
    end
    object BooleanField1: TBooleanField
      DisplayLabel = #25955#35013
      FieldName = 'BULK_FLAG'
    end
  end
end
