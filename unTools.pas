unit unTools;

interface
uses
  Classes, Windows, SysUtils, Forms,  RTLConsts;
  
procedure MessageBox(szCaption, szText: string);
procedure ShowMessage(szText: string);
function BFChoiceDlg(szCaption, szText: string): Boolean;

implementation

procedure MessageBox(szCaption, szText: string);
begin
  Application.MessageBox(PChar(szText), PChar(szCaption), MB_ICONINFORMATION or MB_DEFBUTTON2);
end;

procedure ShowMessage(szText: string);
begin
  Application.MessageBox(PChar(szText), PChar('��Ϣ��ʾ'), MB_ICONINFORMATION or MB_DEFBUTTON2);
end;

function BFChoiceDlg(szCaption, szText: string): Boolean;
begin
  Result := Application.MessageBox(PChar(szText), PChar(szCaption), MB_ICONQUESTION or MB_DEFBUTTON2 or MB_YESNO) = IDYES;
end;
end.
