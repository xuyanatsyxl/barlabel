unit unDm;

interface

uses
  SysUtils, Classes, DB, DBTables, Provider, DBClient;

type
  TUserDm = class(TDataModule)
    LSDB: TDatabase;
    qryGet: TQuery;
  private
    FConnected: Boolean;
    { Private declarations }
  public
    { Public declarations }
    property Connected: Boolean read FConnected write FConnected;
    function getSpinfoByCode(code: string): OleVariant;
  end;

var
  UserDm: TUserDm;

implementation

{$R *.dfm}

function TUserDm.getSpinfoByCode(code: string): OleVariant;
begin
  with qryGet do
  begin
    Close;
    SQL.Clear;
    SQL.Add('select a.SP_ID, SP_CODE, SP_NAME, SHORT_NAME, SPEC, BAR_CODE, HH, UNIT, REG_NAME, SALE_PRICE, d.W_ID, a.BULK_FLAG, c.REG_NAME');
    SQL.Add('from Sp_Info a, Sp_Label b, Region c, Sp_Weigh d');
    SQL.Add('where a.AREA_CODE=c.AREA_CODE and a.LAB_ID=b.LAB_ID and a.SP_ID*=d.SP_ID');
    SQL.Add('and (a.SP_CODE=:code or a.BAR_CODE=:code or a.SP_ID in (select SP_ID from Sp_Weigh where W_ID=convert(int, :code)))');
    ParamByName('code').AsString := code;
    Prepare;
    //cdsGet.Open;
    //Result := cdsGet.Data;
  end;
end;

end.
